from centos:6.6
RUN yum install -y mysql wget
RUN yum install -y tar python unzip dialog
ADD files/init-mysql.sh /usr/bin/init-mysql.sh
CMD ["/usr/bin/init-mysql.sh"]
