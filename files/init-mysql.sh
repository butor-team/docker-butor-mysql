#!/bin/bash
wget https://bitbucket.org/butor-team/butor-stack-setup/get/tip.tar.gz -O /tmp/butor-setup.tar.gz
mkdir -p /tmp/setup/
tar   -xvzf  /tmp/butor-setup.tar.gz -C /tmp/setup
cd $(find /tmp/setup -type d -name "db" | head -1)
./setup-db.sh
clear
echo "Type your mysql root password to run the init script"
mysql -hmysql -uroot -p --default-character-set=utf8 < /tmp/script.sql
