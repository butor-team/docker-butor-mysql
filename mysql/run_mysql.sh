#!/bin/bash
CONTAINER=butor-mysql
RUN=$(docker inspect -f {{.State.Running}} ${CONTAINER})
if [[ ! $? == 0 ]]
then 
	docker run -h mysql --name ${CONTAINER} -e  MYSQL_ROOT_PASSWORD=butorinstall  --volumes-from ${CONTAINER}-data -d mysql:5.5
fi
if [[ "$RUN" == "false" ]] 
then
	docker start ${CONTAINER}
fi
